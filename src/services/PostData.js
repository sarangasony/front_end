export function PostData(type, userData) {
  let BaseURL = "http://localhost:8000/api/";
  let headers = new Headers();

  headers.append("Content-Type", "application/json");
  headers.append("Accept", "application/json");
  const data = JSON.parse(sessionStorage.getItem("userData"));
  if (data && data.api_token) {
    headers.append("Authorization", 'Bearer '+data.api_token);
  }

  return fetch(BaseURL + type, {
    mode: "cors",
    headers,
    method: "POST",
    body: JSON.stringify(userData),
  }).then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      const error = new Error(response.statusText);
      error.code = response.status;
      throw error;
    }
  });
}

export const updateSession = (value) => {
  let prevData = JSON.parse(sessionStorage.getItem('userData'));
  Object.keys(value).forEach(function(val, key){
       prevData[val] = value[val];
  })
  sessionStorage.setItem('userData', JSON.stringify(prevData));
}
