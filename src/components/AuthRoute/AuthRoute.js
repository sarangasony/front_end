import { Route, Redirect } from "react-router-dom";

import { AuthContext } from "../../auth-context";

export const AuthRoute = ({ component: Component, ...rest }) => (
  <AuthContext.Consumer>
    {({ isLogin, setAuth }) => (
      <Route
        {...rest}
        render={(props) =>
          isLogin ? <Component {...props} /> : <Redirect to="/login" />
        }
      />
    )}
  </AuthContext.Consumer>
);
