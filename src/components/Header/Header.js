import React, { Component } from "react";
import "./Header.css";
import { AuthContext } from "../../auth-context";
import { DropdownButton, Dropdown } from 'react-bootstrap';

//function Header() {
class Header extends Component {

  render() {

    let data = JSON.parse(sessionStorage.getItem("userData"));
    let name = ''
    if(data) {
      name = data.username;
    }

    return (
      <AuthContext.Consumer>
        {({ isLogin, setAuth }) => (
          <div>
            <div className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
              <h5 className="my-0 mr-md-auto font-weight-normal">
                Saranga Scopic - Antiques
              </h5>
              {isLogin && (
                <DropdownButton id="dropdown-basic-button" variant="dark" title={name}>
                  <Dropdown.Item href="/autobidding">Auto-bidding</Dropdown.Item>
                  <Dropdown.Divider />
                  <Dropdown.Item href="!#" onClick={() => setAuth(false)} >Log Out</Dropdown.Item>
                </DropdownButton>
              )}
            </div>
          </div>
        )}
      </AuthContext.Consumer>
    );
  }
}

export default Header;
