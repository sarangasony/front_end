import React, { Component } from "react";
import { PostData } from "../../services/PostData";
import "./Login.css";
import { AuthContext } from "../../auth-context";

class Login extends Component {
  constructor() {
    super();

    this.state = {
      username: "",
      password: "",
      error: "",
    };
    this.login = this.login.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  login(setAuth) {

    if (this.state.username && this.state.password) {
      PostData("login", this.state)
        .then((result) => {
          let responseJson = result;
          sessionStorage.setItem("userData", JSON.stringify(responseJson));
          setAuth(true);
          this.props.history.push("/home");
        })
        .catch((error) => {
          this.setState({ error: error.message });
          console.log(error.message);
        });
    }
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    return (
      <AuthContext.Consumer>
        {({ setAuth }) => (
          <div>
            <div className="form-signin">
              <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
              {this.state.error !== "" && (
                <div className="alert alert-danger" role="alert">
                  {this.state.error}
                </div>
              )}
              <label className="sr-only">Email address</label>
              <input
                type="text"
                name="username"
                className="form-control"
                placeholder="Username"
                required
                onChange={this.onChange}
              />
              <label className="sr-only">Password</label>
              <input
                type="password"
                name="password"
                className="form-control"
                placeholder="Password"
                required
                onChange={this.onChange}
              />
              <input
                type="submit"
                className="btn btn-lg btn-dark btn-block"
                value="Login"
                onClick={() => this.login(setAuth)}
              />
            </div>
          </div>
        )}
      </AuthContext.Consumer>
    );
  }
}

export default Login;
