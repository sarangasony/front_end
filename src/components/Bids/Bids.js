import React from 'react';

const bids = (props) => (
  <tr>
    <th scope="row">${props.price}</th>
    <td>{props.createdAt}</td>
  </tr>
)

export default bids;