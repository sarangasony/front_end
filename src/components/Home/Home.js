import React, { Component } from "react";
import "./Home.css";
import { PostData } from "../../services/PostData";
import Products from "../Products/Products"; 
import ReactPaginate from "react-paginate";

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      products: "",
      name: "",
      page: 1,
      priceOrder: "desc",
      queryString: "",
    };

    this.getProducts = this.getProducts.bind(this);
    this.handlePriceOrder = this.handlePriceOrder.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
  }

  componentDidMount() {
    this.getProducts();
  }

  getProducts() {
    let data = JSON.parse(sessionStorage.getItem("userData"));
    this.setState({ name: data.username });
    let postData = {
      user_id: data.id,
      page: this.state.page,
      priceOrder: this.state.priceOrder,
      queryString: this.state.queryString,
    };

    if (data) {
      PostData("home", postData).then((result) => {
        let responseJson = result;
        this.setState({
          ...this.state,
          data: responseJson.data,
          pageCount: responseJson.last_page,
        });
      });
    }
  }

  handlePageClick = (data) => {
   
    let page = data.selected + 1;

    this.setState({ ...this.state, page: page }, () => {
      this.getProducts();
    });
  };

  handlePriceOrder(event) {
    this.setState({ ...this.state, priceOrder: event.target.value }, () => {
      this.getProducts();
    });
  }

  handleSearch(e) {
    this.setState({ ...this.state, queryString: e.target.value }, () => {
      this.getProducts();
    });
  }

  render() {
    return (
      <div className="container">
        <div className="form-inline justify-content-between pb-3">
          <input
            placeholder="Search"
            type="search"
            className="form-control form-control-lg"
            ref={(element) => ((element || {}).onsearch = this.handleSearch)}
          />

          <div className="d-flex">
            <label className="my-1 mr-2" htmlFor="inlineFormCustomSelectPref">
              Sort by price
            </label>
            <select
              id="inlineFormCustomSelectPref"
              value={this.state.priceOrder}
              className="form-control"
              onChange={this.handlePriceOrder}
            >
              <option value="asc">Accending</option>
              <option value="desc">Desending</option>
            </select>
          </div>
        </div>

        <div className="card-deck mb-3 text-center">
          <Products
            productData={this.state.data}
          />
        </div>

        <ReactPaginate
          previousLabel={"previous"}
          nextLabel={"next"}
          breakLabel={"..."}
          breakClassName={"page-link"}
          pageCount={this.state.pageCount}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={this.handlePageClick}
          containerClassName={"pagination"}
          pageClassName={"page-item"}
          pageLinkClassName={"page-link"}
          previousClassName={"page-item"}
          previousLinkClassName={"page-link"}
          nextClassName={"page-item"}
          nextLinkClassName={"page-link"}
          activeClassName={"active"}
        />
      </div>
    );
  }
}

export default Home;
