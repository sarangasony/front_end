import React, { Component } from "react";
import "./Detail.css";
import { PostData } from "../../services/PostData";
import Countdown from 'react-countdown';
import Bids from '../Bids/Bids';

class Detail extends Component {

  constructor(props) {
    super(props);

    const query = new URLSearchParams(this.props.location.search);
    const productId = query.get("p-id");

    this.state = {
      data: null,
      productId : productId,
      maxPrice: 0,
      userPrice: 0,
      userId : null,
      autobidding : 0,
      bidsHistory : []
    };

    this.getProduct = this.getProduct.bind(this);
   
  }

  componentDidMount() {
    this.getProduct();
  }

  getProduct() {
    
    let data = JSON.parse(sessionStorage.getItem("userData"));
    this.setState({ name: data.username });
    let postData = {
      user_id: data.id,
      product_id: this.state.productId,
    };

    this.setState({
      ...this.state,
      userId:  data.id
    });

    PostData("product", postData).then((result) => {

      let price = 0;
      const bids = result.bids;
      let user_auto_bid = [];
      if(bids) {
        for (var i=0 ; i<bids.length ; i++) {
            if (price == null || parseInt(bids[i]['bid_price']) > parseInt(price))
              price = bids[i]['bid_price'];
            if (bids[i]['user_id'] === this.state.userId ) {
              user_auto_bid[this.state.userId] = bids[i]['auto_bidding'];
            }  
        }
      }

      this.setState({bidsHistory :  result.bids });

      if(user_auto_bid){
        this.setState({autobidding:  user_auto_bid[this.state.userId] });
      }

      if(price < result.product.price) {
        price = result.product.price;
      }

      this.setState({
        ...this.state,
        data: result.product,
        maxPrice: price + 1,
        userPrice: price + 1,
      });
    });
  }

  backToHomeHandler = () => {
    this.props.history.push("/home");
  }

  priceChangeHandler = (event) => {

    let price = 0;
    if(event.target.value) {
      price = parseFloat(event.target.value)
    }

    this.setState({
      userPrice: price
    });
  }

  autoBiddingHandler = (event) => {
    let autobidding = 0;
    if(event.target.checked) {
      autobidding = 1;
    }
    this.setState({autobidding : autobidding});
  }

  submitBidHandler = () => {
    if(this.state.userPrice < this.state.maxPrice) {
      alert('Price should greater than '+ this.state.maxPrice );
      return false;
    }


    let postData = {
      user_id: this.state.userId,
      product_id: this.state.productId,
      price: this.state.userPrice,
      auto_bidding:  this.state.autobidding
    };

    PostData("bid", postData).then((result) => {
      alert(result.message); 
    });
  }
  
  render () {
    let product = <p style={{textAlign: 'center'}}>Please select a product</p>;
    let showSubmitButton = true;

    if(this.state.productId) {
      product = <p style={{textAlign: 'center'}}>Loading...</p>;
    }

    if(this.state.data) {
      const Completionist = () => <span>Bid closed</span>;
      const renderer = ({ days, hours, minutes, seconds, completed }) => {
        if (completed) {
          showSubmitButton = false;
          return <Completionist />;
        } else {
          return <span><p>BID IS CLOSE IN</p>{days} days {hours} hours {minutes} minutes {seconds} seconds</span>;
        }
      };
      const closedate = this.state.data.close_date_time;

      const bidHistoryObject = Object.assign([], this.state.bidsHistory).reverse();
      const bidsHistory = bidHistoryObject.map( ( bid, index) => {
        return <Bids price={bid.bid_price} createdAt={bid.created_at}  key={bid.id}  />
      });
      

      product = (
        <div className="container">

          <div className="card">
            <div className="row">
              
              <aside className="col-sm-12">
                <article className="card-body p-5 clearfix">

                  <div className="alert alert-warning" role="alert">
                    
                    <h3 className="title" style={{marginBottom: '0', textAlign: 'center'}} ><Countdown date={Date.parse(closedate)}  renderer={renderer} /></h3>
                  </div>

                
                  <h3 className="title mb-3">{this.state.data.name}</h3>

                  <p className="price-detail-wrap"> 
                    <span className="price h3 text-warning"> 
                      <span className="currency">US $</span><span className="num">{this.state.data.price}</span>
                    </span>
                  </p>

                  <div className="row">
              
                    <div className="col-sm-6">
                      <dl className="item-property">
                        <dt>Description</dt>
                        <dd><p>{this.state.data.description}</p></dd>
                      </dl>
                      <dl className="param param-feature">
                        <dt>Model#</dt>
                        <dd>{this.state.data.id}</dd>
                      </dl> 
                      <dl className="param param-feature">
                        <dt>Close date and time</dt>
                        <dd>{this.state.data.close_date_time}</dd>
                      </dl> 

                      <dl className="param param-feature">
                        <dt>Bid Amount</dt>
                        <dd><input placeholder="Bid Amount" type="text" className="col-sm-6  col-md-4 col-lg-3 form-control" value={this.state.userPrice} onChange={this.priceChangeHandler}/></dd>
                      </dl> 

                      <dl className="param param-feature">
                        <dt>Auto-bidding</dt>
                        <dd><input type="checkbox" className="bg-dark" checked = {this.state.autobidding?'checked':''} onChange={this.autoBiddingHandler} /></dd>
                      </dl> 
                    </div>

                    <div className="col-sm-6">
                      <dt>Bid History</dt>
                      <div className="scrollTable">
                        <table className="table">
                          <thead>
                            <tr>
                              <th scope="col">Price</th>
                              <th scope="col">Date</th>
                            </tr>
                          </thead>
                          <tbody>
                            { Object.keys(this.state.bidsHistory).length === 0?<tr><td colSpan="2">No records found</td></tr>:bidsHistory}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <button className="btn btn-lg btn-secondary float-right" onClick={() => this.backToHomeHandler()}>Go Back</button>
                  {
                  showSubmitButton && 
                  <button className="btn btn-lg btn-dark" onClick={() => this.submitBidHandler()}>Submit Bid</button>
                  }
                  
                 
                </article> 
              </aside> 
            </div>
          </div>



        </div>


      )
    }

    return product;
  }
}

export default Detail;
