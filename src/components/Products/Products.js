import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import Item from "../Item/Item";
import "./Products.css";

class Products extends Component {

  productSelectedHandler = (id) => {
    this.props.history.push("/detail?p-id=" + id);
  }

  render() {

    const products = this.props.productData.map( (productData, index) => {
      return <Item 
        name={productData.name} 
        price={productData.price} 
        description={productData.description} 
        key={productData.id} 
        clicked={() => this.productSelectedHandler(productData.id)} />
    });


    return (
      <>{products}</>
    );

  }
}

export default withRouter(Products);
