import React, { Component } from "react";
import "./Autobidding.css";
import { PostData, updateSession} from "../../services/PostData";

class Autobidding extends Component {
  constructor(props) {
    super(props);

    this.state = {
      maxBidAmount: 0,
      userId: null
    };

    this.getMaxBidAmount = this.getMaxBidAmount.bind(this);
  }

  componentDidMount() {
    this.getMaxBidAmount();
  }

  getMaxBidAmount() {
    let data = JSON.parse(sessionStorage.getItem("userData"));
    this.setState({ maxBidAmount: data.maximum_bid_amount , userId: data.id });
  }

  bidChangeHandler = (event) => {

    let maxBidAmount = 0;
    if(event.target.value) {
      maxBidAmount = parseFloat(event.target.value)
    }

    this.setState({
      maxBidAmount: maxBidAmount
    });
  }

  maxBidAmountHandler = () => {
    
    let postData = {
      user_id: this.state.userId,
      maximum_bid_amount: this.state.maxBidAmount
    };

    PostData("maxBidAmount", postData).then((result) => {
      if(result.success) {
        updateSession({maximum_bid_amount: this.state.maxBidAmount });
      }
      
      alert(result.message); 
    });
  }

  render() {
    return (
      <div className="container">
      
      <dl className="param param-feature">
        <h1>Maximum bid amount</h1>
        <dd><input placeholder="Bid Amount" type="text" className="col-sm-6  col-md-4 col-lg-3 form-control form-control-lg mb-5" value={this.state.maxBidAmount === 0 ? '':this.state.maxBidAmount} onChange={this.bidChangeHandler}/></dd>
      </dl> 

      <button className="btn btn-lg btn-secondary float-right" onClick={this.props.history.goBack}>Back to bid</button>
      <button className="btn btn-lg btn-dark" onClick={() => this.maxBidAmountHandler()}>Save</button>    
        
      </div>
    );
  }
}

export default Autobidding;
