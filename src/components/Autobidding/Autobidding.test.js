import { render, screen } from "@testing-library/react";
import Autobidding from "./Autobidding";

test("Autobidding renders learn react link", () => {
  render(<Autobidding />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
