import React from 'react';

const item = (props) => (

    <div className="card mb-4 shadow-sm">
      <div className="card-header">
        <h4 className="my-0 font-weight-normal">{props.name}</h4>
      </div>
      <div className="card-body">
        <h1 className="card-title pricing-card-title">
          <small className="text-muted">$</small>
          {props.price}{" "}
        </h1>
        <div className="list-unstyled mt-3 mb-4">
          {props.description}
        </div>
        <button
          type="button"
          className="btn btn-lg btn-block btn-outline-dark"
          onClick = {props.clicked}
        >
          Bid Now
        </button>
      </div>
    </div>
    
)

export default item;