import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import { AuthRoute } from "./components/AuthRoute/AuthRoute";
import Login from "././components/Login/Login";
import Home from "././components/Home/Home";
import Detail from "././components/Detail/Detail";
import Autobidding from "././components/Autobidding/Autobidding";

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/login" component={Login} />
      <AuthRoute path="/home" component={Home} />
      <AuthRoute path="/detail" component={Detail} />
      <AuthRoute path="/autobidding" component={Autobidding} />
      <Redirect to="/login" />
    </Switch>
  </BrowserRouter>
);

export default Routes;
