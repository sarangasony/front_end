import React, { Component } from "react";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Routes from "./routes";
import "./App.css";
import "./styles/bootstrap.min.css";
import { AuthContext } from "./auth-context";

class App extends Component {
  constructor(props) {
    super(props);

    this.setAuth = (islogin) => {
      this.setState((state) => ({
        isLogin: islogin,
      }));
      
      if (!islogin) {
        sessionStorage.removeItem("userData");
        sessionStorage.clear();
      }
    };

    this.state = {
      isLogin: sessionStorage.getItem("userData") ? true : false,
      setAuth: this.setAuth,
    };
  }

  render() {
    return (
      <div className="App">
        <AuthContext.Provider value={this.state}>
          <Header value={this.state} />
          <Routes />
          <Footer />
        </AuthContext.Provider>
      </div>
    );
  }
}

export default App;
